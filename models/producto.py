from models.proveedor import ProveedorModel
from models.categoria import CategoriaModel
from flask_restful.reqparse import Namespace
from db import db

from utils import _assign_if_something

class ProductoModel(db.Model):
    __tablename__ = 'producto'

    id = db.Column(db.BigInteger, primary_key = True)
    nombre = db.Column(db.String)
    descripcion = db.Column(db.String)
    precio = db.Column(db.Integer)
    estado = db.Column(db.String)
    categoria_id = db.Column(db.BigInteger, db.ForeignKey(CategoriaModel.id))
    proveedor_id = db.Column(db.BigInteger, db.ForeignKey(ProveedorModel.id))


    _categoria = db.relationship('CategoriaModel', uselist=False, primaryjoin='CategoriaModel.id == ProductoModel.categoria_id', foreign_keys='ProductoModel.categoria_id')
    _proveedor = db.relationship('ProveedorModel', uselist=False, primaryjoin='ProveedorModel.id == ProductoModel.proveedor_id', foreign_keys='ProductoModel.proveedor_id')

    def __init__(self, id, nombre, descripcion, precio, estado, categoria_id, proveedor_id):
        self.id = id
        self.nombre = nombre
        self.descripcion = descripcion
        self.precio = precio
        self.estado = estado

        self.categoria_id = categoria_id
        self.proveedor_id = proveedor_id
    
    def json(self, jsondepth = 0):
        json = {
            'id': self.id,
            'nombre': self.nombre,
            'descripcion': self.descripcion,
            'precio': self.precio,
            'estado': self.estado,
            'categoria_id': self.categoria_id,
            'proveedor_id': self.proveedor_id
        }

       

        if jsondepth > 0:
            if self._categoria:
                json['_categoria'] = self._categoria.json(jsondepth)
            if self._proveedor:
                json['_proveedor'] = self._proveedor.json(jsondepth)    
        return json
        
#
#
    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['nombre','descripcion','estado','categoria_id','proveedor_id','precio']:
            _assign_if_something(self, newdata, no_pk_key)