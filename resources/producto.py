import logging
from flasgger import swag_from
from flask import request
from models.producto import ProductoModel
from flask_restful import Resource, reqparse

from utils import paginated_results, restrict

class Producto(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type = int)
    parser.add_argument('nombre', type = str)
    parser.add_argument('descripcion', type = str)
    parser.add_argument('precio', type = int)
    parser.add_argument('estado', type = str)
    parser.add_argument('categoria_id', type = int)
    parser.add_argument('proveedor_id', type = int)


    @swag_from('../swagger/producto/get_producto.yaml')
    def get(self, id): 
        producto = ProductoModel.find_by_id(id)
        if producto:
            return producto.json()

        return {'message': 'No se encuentra el producto'}, 404

    @swag_from('../swagger/producto/put_producto.yaml')
    def put(self, id):
        producto = ProductoModel.find_by_id(id)
        if producto:
            newdata = Producto.parser.parse_args()
            producto.from_reqparse(newdata)
            producto.save_to_db()
            return producto.json()

        return {'message': 'No se encuentra el producto'}, 404
    
    @swag_from('../swagger/producto/delete_producto.yaml')
    def delete(self, id):
        producto = ProductoModel.find_by_id(id)
        if producto:
            producto.delete_from_db()

        return {'message': 'Se ha borrado el producto'}
        
class ProductoList(Resource):
    @swag_from('../swagger/producto/list_producto.yaml')
    def get(self):
        query = ProductoModel.query
        return paginated_results(query)

    @swag_from('../swagger/producto/post_producto.yaml')
    def post(self):
        data = Producto.parser.parse_args()

        id = data.get('id')

        if id is not None and ProductoModel.find_by_id(id):
            return {'message': "Ya existe un producto con el id"}, 404
        
        task = ProductoModel(**data)
        try:
            task.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error al crear Tarea.', exc_info=e)
            return {'message': "Ocurrio un error al crear la Tarea"}, 500
        return task.json(), 201

class ProductoSearch(Resource):
    @swag_from('../swagger/producto/search_producto.yaml')
    def post(self):
        query = ProductoModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id', lambda x: ProductoModel.id == x)
            query = restrict (query, filters,'nombre', lambda x: ProductoModel.nombre == x)
            query = restrict (query, filters,'descripcion', lambda x: ProductoModel.descripcion == x)
            query = restrict (query, filters,'precio', lambda x: ProductoModel.precio == x)
            query = restrict (query, filters,'estado', lambda x: ProductoModel.estado == x)
            query = restrict (query, filters,'categoria_id', lambda x: ProductoModel.estado == x)
            query = restrict (query, filters,'proveedor_id', lambda x: ProductoModel.estado == x)
       
        return paginated_results(query)