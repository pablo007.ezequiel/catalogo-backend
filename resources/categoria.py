import logging
from flasgger import swag_from
from flask import request
from models.categoria import CategoriaModel
from flask_restful import Resource, reqparse



from utils import paginated_results, restrict

class Categoria(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type = int)
    parser.add_argument('descripcion', type = str)
   



    @swag_from('../swagger/categoria/get_categoria.yaml')
    def get(self, id):
        categoria = CategoriaModel.find_by_id(id)
        if categoria:
            return categoria.json()

        return {'message': 'No se encuentra la Categoria'}, 404

    @swag_from('../swagger/categoria/put_categoria.yaml')
    def put(self, id):
        categoria = CategoriaModel.find_by_id(id)
        if categoria:
            newdata = Categoria.parser.parse_args()
            categoria.from_reqparse(newdata)
            categoria.save_to_db()
            return categoria.json()

        return {'message': 'No se encuentra la Categoria'}, 404
    
    @swag_from('../swagger/categoria/delete_categoria.yaml')
    def delete(self, id):
        categoria = CategoriaModel.find_by_id(id)
        if categoria:
            categoria.delete_from_db()

        return {'message': 'Se ha borrado la Categoria'}
        
class CategoriaList(Resource):
    @swag_from('../swagger/categoria/list_categoria.yaml')
    def get(self):
        query = CategoriaModel.query
        return paginated_results(query)

    @swag_from('../swagger/categoria/post_categoria.yaml')
    def post(self):
        data = Categoria.parser.parse_args()

        id = data.get('id')

        if id is not None and CategoriaModel.find_by_id(id):
            return {'message': "Ya existe una categoria con el id"}, 404
        
        proveedor = CategoriaModel(**data)
        try:
            proveedor.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error en la categoria.', exc_info=e)
            return {'message': "Ocurrio un error al crear una categoria"}, 500
        return proveedor.json(), 201

class CategoriaSearch(Resource):
    @swag_from('../swagger/categoria/search_categoria.yaml')
    def post(self):
        query = CategoriaModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id', lambda x: CategoriaModel.id == x)
            query = restrict (query, filters,'nombre', lambda x: CategoriaModel.descripcion == x)
        return paginated_results(query)