import logging
from flasgger import swag_from
from flask import request
from models.proveedor import ProveedorModel
from flask_restful import Resource, reqparse



from utils import paginated_results, restrict

class Proveedor(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type = int)
    parser.add_argument('nombre', type = str)
    parser.add_argument('direccion', type = str)
    parser.add_argument('telefono', type = str)



    @swag_from('../swagger/proveedor/get_proveedor.yaml')
    def get(self, id):
        proveedor = ProveedorModel.find_by_id(id)
        if proveedor:
            return proveedor.json()

        return {'message': 'No se encuentra el proveedor'}, 404

    @swag_from('../swagger/proveedor/put_proveedor.yaml')
    def put(self, id):
        proveedor = ProveedorModel.find_by_id(id)
        if proveedor:
            newdata = Proveedor.parser.parse_args()
            proveedor.from_reqparse(newdata)
            proveedor.save_to_db()
            return proveedor.json()

        return {'message': 'No se encuentra el proveedor'}, 404
    
    @swag_from('../swagger/proveedor/delete_proveedor.yaml')
    def delete(self, id):
        proveedor = ProveedorModel.find_by_id(id)
        if proveedor:
            proveedor.delete_from_db()

        return {'message': 'Se ha borrado el proveedor'}
        
class ProveedorList(Resource):
    @swag_from('../swagger/proveedor/list_proveedor.yaml')
    def get(self):
        query = ProveedorModel.query
        return paginated_results(query)

    @swag_from('../swagger/proveedor/post_proveedor.yaml')
    def post(self):
        data = Proveedor.parser.parse_args()

        id = data.get('id')

        if id is not None and ProveedorModel.find_by_id(id):
            return {'message': "Ya existe un proveedor con el id"}, 404
        
        proveedor = ProveedorModel(**data)
        try:
            proveedor.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error el proveedor.', exc_info=e)
            return {'message': "Ocurrio un error al crear el proveedor"}, 500
        return proveedor.json(), 201

class ProveedorSearch(Resource):
    @swag_from('../swagger/proveedor/search_proveedor.yaml')
    def post(self):
        query = ProveedorModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id', lambda x: ProveedorModel.id == x)
            query = restrict (query, filters,'nombre', lambda x: ProveedorModel.nombre == x)
            query = restrict (query, filters,'direccion', lambda x: ProveedorModel.direccion == x)
            query = restrict (query, filters,'telefono', lambda x: ProveedorModel.telefono == x)
        return paginated_results(query)